/*
  run-diag.c - run a diagnostic with form input

  (C)2017 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>


#include <infratest.h>


FILE
  *logfile;


int
  main
    (int
      argc,
    char *
      argv []);


/*
  globally accessed parameters
*/
int
  param_debug;
int
  param_identity;
char
  param_dut_fqdn [1024];

int
  main
    (int
      argc,
    char *
      argv [])

{ /* main for run-diag.c */

  char arguments [1024];
  char command [2*1024];
  int do_output;
  int status;


  status = ST_OK;
  param_identity = 0;
  if (0 == strcmp ("/tester/current/cgi-bin/run-diag-1202", argv [0]))
    param_identity = 801;
  param_debug = 1;
  strcpy (arguments, getenv ("QUERY_STRING"));
  if (0 == strncmp ("dut_fqdn=", arguments, strlen ("dut_fqdn=")))
    strcpy (param_dut_fqdn, arguments+strlen("dut_fqdn="));

  do_output = 1;
  logfile = NULL;
  if (param_debug > 0)
  {
    logfile = fopen ("/tester/current/log2/model.log", "a");
    fprintf (logfile, "%s(%d) started. DUT FQDN=%s\n",
      argv [0], param_identity, param_dut_fqdn);
  };

  if (do_output)
  {
    printf ("Content-type: text/html\n\n");
    if (param_debug > 0)
      fprintf (logfile, "query: %s\n", arguments);

    switch (param_identity)
    {
    case 801:
      sprintf (command,
        "sudo -n /tester/current/bin/diag/diag1202/tlsdiag tls_diag %s", param_dut_fqdn);
      if (param_debug > 0)
        fprintf (logfile, "command will be %s\n", command);
      system (command);
      printf ("<HTML><TITLE>TLS+Cert Diagnostic A</TITLE><BODY>\n");
      printf ("<H1>TLS+Cert A</H1>\n");
      printf ("<A HREF=\"/results/diag/diag1202/reporter_01.pdf\">Report</A>.\n");
      printf (
        "<PRE>Diagnostic completed.</PRE></BODY></HTML>\n");
      break;
    default:
      printf ("<HTML><TITLE>Diagnostic</TITLE><BODY>\n");
      printf (
        "<PRE>Diagnostic completed.</PRE></BODY></HTML>\n");
      ;
    };
  };

  if (logfile != NULL)
    fclose (logfile);
  return (status);

} /* main for run-diag.c */

