/*
  tester-status.c - display tester status and current config

  (C)2017-2018 Smithee Solutions LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include <infratest.h>


/*
  globally accessed parameters
*/
int
  param_debug;
FILE
  *logfile;


int
  get_status
    (int
      option)

{ /* get_status */

  char
    buffer [1024];
  FILE
    *f;
  char
    filename [1024];
  int
    status;
  char
    *status_gets;


  status = ST_OK;
  sprintf (filename,
    "/%s/etc/options/%03d/status.txt", TROOT, option);

  f = fopen (filename, "r");
  if (f != NULL)
  {
    status_gets = fgets (buffer, sizeof (buffer), f);
  }
  else
  {
    status_gets = NULL;
  };
  if ((status_gets != NULL) && (strlen (buffer) < (sizeof(buffer)-8)))
  {
    printf ("%s\n", buffer);
  }
  else
  {
    printf ("status retrieval error\n");
  };
  if (f != NULL)
    fclose (f);

  return (status);

} /* get_status */


int
  main
    (int
      argc,
    char *
      argv [])

{ /* main for tester-status.c */

  char command [1024];
  int do_output;
  char filename [1024];
  int status;


  status = ST_OK;
  param_debug = 1;
  do_output = 1;
  logfile = NULL;
  sprintf (filename, "/%s/log2/model.log", TROOT);
  if (param_debug > 0)
    logfile = fopen (filename, "a");

  if (do_output)
  {
    printf ("Content-type: text/html\n\n");

    printf ("<HTML><TITLE>Tester Status</TITLE><BODY>\n");
    printf ("(back to <A HREF=\"/\">Main</A> menu.)<BR>\n");
    sprintf (command, "/%s/bin/tester-status-top", TROOT);
    fflush (stdout);system (command);

    fflush (stdout);
    printf ("<PRE>\n");
    sprintf (command, "/%s/bin/enumerate-parameters", TROOT);
    system (command);
    printf ("</PRE>\n");

    printf ("</BODY></HTML>\n");
  };

  if (logfile != NULL)
    fclose (logfile);
  return (status);

} /* main for tester-status.c */

