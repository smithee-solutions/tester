/*
  execute-action.c - executes a controlled set of actions

  (C)2017 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include <infratest.h>


FILE
  *logfile;


int
  main
    (int
      argc,
    char *
      argv []);


/*
  globally accessed parameters
*/
int
  param_debug;


int
  main
    (int
      argc,
    char *
      argv [])

{ /* main for execute-action.c */

  char
    arguments [1024];
  char
    command [1024];
  int
    do_output;
  char
    filename [1024];
  int
    status;


  status = ST_OK;
  param_debug = 1;
  do_output = 1;
  logfile = NULL;
  if (param_debug > 0)
  {
    sprintf (filename, "/%s/log2/model.log", TROOT);
    logfile = fopen (filename, "a");
  };

  if (do_output)
  {
    printf ("Content-type: text/html\n\n");
    strcpy (arguments, getenv ("QUERY_STRING"));
    if (strlen (arguments) > 0)
    {
      int
        action_number;

char tmp1 [1024];
      strncpy (tmp1, arguments+strlen("action="), 4);
      if (param_debug > 0)
        fprintf (logfile, "arguments: (raw %s) %s\n", tmp1, arguments);
      sscanf (arguments + strlen ("action="), "%d", &action_number);
      sprintf (command, "/%s/bin/model/action-%03d",
        TROOT, action_number);
      if (param_debug > 0)
        fprintf (logfile, "executing %s\n", command);
      printf ("<HTML><TITLE>Execute Action</TITLE><BODY>\n");
      printf ("<h1>FINISHED</h1>\n");
      printf ("<PRE>Executing %s.</PRE>\n", command);
      printf ("<A HREF=\"/\">Back</A> to main menu.\n");
      printf ("</BODY></HTML>\n");
      fflush (stdout);
      system (command);
    };
  };


  if (logfile != NULL)
    fclose (logfile);
  return (status);

} /* main for control-tftp.c */

