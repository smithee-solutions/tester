# Makefile

# (C)2017 Smithee Solutions LLC

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TROOT = opt/tester
DOC = ${TROOT}/doc/tester-operating-manual.pdf
WWW = ${TROOT}/www/index.html
TESTER_DIRS = ${TROOT}/bin/diag/diag1091 \
	${TROOT}/bin/model \
	${TROOT}/cgi-bin \
	${TROOT}/doc \
	${TROOT}/download \
	${TROOT}/etc/apache2/sites-enabled \
	${TROOT}/etc/bind \
	${TROOT}/etc/dhcp \
	${TROOT}/etc/param \
	${TROOT}/etc/preset1 \
	${TROOT}/etc/preset2 \
	${TROOT}/etc/preset9 \
	${TROOT}/etc/sudoers.d \
	${TROOT}/log \
	${TROOT}/log2 \
	${TROOT}/results \
	${TROOT}/run \
	${TROOT}/setup \
	${TROOT}/tmp \
	${TROOT}/www \
	${TROOT}/www/control \
	${TROOT}/www/diag

all:	${DOC} ${WWW}

clean:
	rm -rf ${TESTER_DIRS}
	rm -rf opt
	(cd bin-src; make clean; cd ..)
	(cd bin-model-src; make clean; cd ..)
	(cd cgi-bin-src; make clean; cd ..)
	(cd etc-src; make clean; cd ..)
	(cd www-src; make clean; cd ..)

build:	${DOC} ${TESTER_DIRS} ${WWW}
	(mkdir -p ${TROOT}/include; cp include/infratest.h ${TROOT}/include)
	cp doc-src/tester-operating-manual.pdf ${TROOT}/doc
	(cd bin-src; make build; cd ..)
	(cd bin-diag-src; make build; cd ..)
	(cd bin-model-src; make build; cd ..)
	(cd cgi-bin-src; make build; cd ..)
	(cd etc-src; make build; cd ..)
	(cd www-src; make build; cd ..)

make-dirs:
	mkdir -p ${TESTER_DIRS}
	ln -s ../results ${TROOT}/www/results
	chmod 777 ${TROOT}/log
	chmod 777 ${TROOT}/results
	chmod 777 ${TROOT}/tmp
	chmod 777 ${TROOT}/log2

${TROOT}/bin/diag/diag1091:	make-dirs

${TROOT}/bin/model:	make-dirs

${TROOT}/cgi-bin:	make-dirs

${TROOT}/doc:	make-dirs

${TROOT}/etc:	make-dirs

${TROOT}/log:	make-dirs

${TROOT}/www:	make-dirs

${TROOT}/doc/tester-operating-manual.pdf:	doc-src/tester-operating-manual.pdf ${TESTER_DIRS}
	cp doc-src/tester-operating-manual.pdf ${TROOT}/doc

${TROOT}/www/index.html:	www-src/index.html
	cp www-src/index.html ${TROOT}/www

